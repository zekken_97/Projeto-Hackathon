-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 10, 2018 at 01:22 AM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hackathon`
--

-- --------------------------------------------------------

--
-- Table structure for table `agente`
--

DROP TABLE IF EXISTS `agente`;
CREATE TABLE IF NOT EXISTS `agente` (
  `IdAgente` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(250) NOT NULL,
  `Descricao` text NOT NULL,
  `UltimaLongitude` float NOT NULL,
  `UltimaLatitude` float NOT NULL,
  PRIMARY KEY (`IdAgente`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `informacoes_ocorrencia`
--

DROP TABLE IF EXISTS `informacoes_ocorrencia`;
CREATE TABLE IF NOT EXISTS `informacoes_ocorrencia` (
  `IdInforOcorrencia` int(11) NOT NULL AUTO_INCREMENT,
  `NomePessoa` varchar(250) NOT NULL,
  `Email` varchar(250) NOT NULL,
  `CPF` int(20) NOT NULL,
  PRIMARY KEY (`IdInforOcorrencia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `localizacao`
--

DROP TABLE IF EXISTS `localizacao`;
CREATE TABLE IF NOT EXISTS `localizacao` (
  `IdLocalizacao` int(11) NOT NULL AUTO_INCREMENT,
  `Bairro` varchar(250) NOT NULL,
  `Numero` int(11) NOT NULL,
  `Complementos` varchar(250) NOT NULL,
  `CEP` int(11) NOT NULL,
  PRIMARY KEY (`IdLocalizacao`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ocorrencia`
--

DROP TABLE IF EXISTS `ocorrencia`;
CREATE TABLE IF NOT EXISTS `ocorrencia` (
  `IdOcorrencia` int(11) NOT NULL AUTO_INCREMENT,
  `IdLocalizacao` int(11) NOT NULL,
  `IdInforOcorrencia` int(11) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `IdAgente` int(11) NOT NULL,
  `Descricao` text NOT NULL,
  `Nivel` int(11) NOT NULL,
  `Situacao` varchar(250) NOT NULL,
  `DataInclusao` datetime NOT NULL,
  PRIMARY KEY (`IdOcorrencia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `IdUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(250) NOT NULL,
  `Email` varchar(250) NOT NULL,
  `Senha` varchar(250) NOT NULL,
  `CPF` int(20) NOT NULL,
  `Sexo` char(1) NOT NULL,
  `Idade` int(11) NOT NULL,
  `Telefone` int(15) NOT NULL,
  PRIMARY KEY (`IdUsuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
