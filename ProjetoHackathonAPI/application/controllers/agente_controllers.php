<?php
class agente extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('agente_model');
		$this->load->helper('url_helper');
	}

	public function index(){

		$data['agente'] = $this->agente_model->Get();
		$data['title'] = 'Todos as agente';

		$this->load->view('templates/header', $data);
		$this->load->view('agente/index', $data);
		$this->load->view('templates/footer', $data);

	}

	public function view($uri){
		$data['agente_item'] = $this->agente_model->Get($uri);

		if(empty($data['agente_item'])){
			show_404();
		}

		$data['title'] = $data['agente_item']['title'];

		$this->load->view('templates/header', $data);
		$this->load->view('agente/view', $data);
		$this->load->view('templates/footer', $data);

	}

	public function create(){

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Criar um novo agente';

		if($this->form_validation->run() === false){
			$this->load->view('templates/header', $data);
			$this->load->view('agente/create');
			$this->load->view('templates/footer');
		}else{
			$this->news_model->Set();
			$this->load->view('agente/success');
		}

	}

}