<?php
class ocorrencia_controller extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('ocorrencia_model');
		$this->load->helper('url_helper');
	}

	public function index(){

		$data['ocorrencia'] = $this->ocorrencia_model->Get();
		$data['title'] = 'Todos as ocorrencia';

		$this->load->view('templates/header', $data);
		$this->load->view('ocorrencia/index', $data);
		$this->load->view('templates/footer', $data);

	}

	public function view($uri){
		$data['ocorrencia_item'] = $this->ocorrencia_model->Get($uri);

		if(empty($data['ocorrencia_item'])){
			show_404();
		}

		$data['title'] = $data['ocorrencia_item']['title'];

		$this->load->view('templates/header', $data);
		$this->load->view('ocorrencia/view', $data);
		$this->load->view('templates/footer', $data);

	}

	public function create(){

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Criar uma nova ocorrência';

		// $this->form_validation->set_rules('title', 'Titulo', 'required');
		// $this->form_validation->set_rules('text', 'Texto', 'required');

		if($this->form_validation->run() === false){
			$this->load->view('templates/header', $data);
			$this->load->view('ocorrencia/create');
			$this->load->view('templates/footer');
		}else{
			$this->news_model->Set();
			$this->load->view('ocorrencia/success');
		}

	}

}