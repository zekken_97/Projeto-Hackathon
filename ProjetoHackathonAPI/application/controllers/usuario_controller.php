<?php
class usuario_controller extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('usuario_model');
		//$this->load->helper('url_helper');	
	}

	public function index(){

		$data['usuario'] = $this->usuario_model->Get();
		$data['title'] = 'Todos os usuarios';

	}

	public function view($uri = null){
		$data['usuario_item'] = $this->usuario_model->Get($uri);
		$data = json_encode($data['usuario_item']);

		echo $data;
	}

	public function create(){
		return  'true';
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Criar um novo usuario';

		if($this->form_validation->run() === false){

		}else{
			$this->news_model->Set();
			$this->load->view('usuario/success');
		}

	}

}