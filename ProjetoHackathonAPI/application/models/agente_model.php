<?php

class agente_model extends CI_Model{

    public function __construct(){
		$this->load->database();
	}

	public function Get($id = false){
		if($id === false){
			$query = $this->db->get('agente'); 
			return $query->result_array();
		}else{
			$query = $this->db->get_where("agente", array('IdAgente'=>$id));
			return $query->row_array();
		}
	}

	public function Set(){
        $this->load->helper('agente');
        
		$data = array(
			'Nome' => $this->input->post('Nome'),
			'Descricao' => $this->input->post('Descricao'),
			'UltimaLongitude' => $this->input->post('UltimaLongitude'),
			'UltimaLatitude' => $this->input->post('UltimaLatitude')
		);

		$this->db->insert('agente', $data);

	}
}

?>