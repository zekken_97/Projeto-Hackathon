<?php

class informacoes_ocorrencia_model extends CI_Model{

    public function __construct(){
		$this->load->database();
	}

	public function Get($id = false){
		if($id === false){
			$query = $this->db->get('informacoes_ocorrencia'); 
			return $query->result_array();
		}else{
			$query = $this->db->get_where("informacoes_ocorrencia", array('IdInforOcorrencia'=>$id));
			return $query->row_array();
		}
	}

	public function Set(){
        $this->load->helper('agente');
        
		$data = array(
			'NomePessoa' => $this->input->post('NomePessoa'),
			'Email' => $this->input->post('Email'),
			'CPF' => $this->input->post('CPF')
		);

		$this->db->insert('agente', $data);

	}
}

?>