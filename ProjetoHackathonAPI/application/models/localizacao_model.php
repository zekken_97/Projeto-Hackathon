<?php

class localizacao_model extends CI_Model{

    public function __construct(){
		$this->load->database();
	}

	public function Get($id = false){
		if($id === false){
			$query = $this->db->get('localizacao'); 
			return $query->result_array();
		}else{
			$query = $this->db->get_where("localizacao", array('IdLocalizacao'=>$id));
			return $query->row_array();
		}
	}

	public function Set(){
        $this->load->helper('localizacao');
        
		$data = array(
			'Numero' => $this->input->post('Numero'),
			'Bairro' => $this->input->post('Bairro'),
			'Complementos' => $this->input->post('Complementos'),
			'CEP' => $this->input->post('CEP')
		);

		$this->db->insert('localizacao', $data);

	}
}

?>