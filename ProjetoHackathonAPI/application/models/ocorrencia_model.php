<?php

class ocorrencia_model extends CI_Model{

    public function __construct(){
		$this->load->database();
	}

	public function Get($id = false){
		if($id === false){
			$query = $this->db->get('ocorrencia'); 
			return $query->result_array();
		}else{
			$query = $this->db->get_where("ocorrencia", array('IdOcorrencia'=>$id));
			return $query->row_array();
		}
	}

	public function Set(){
        $this->load->helper('ocorrencia');
        
		$data = array(
			'IdLocalizacao' => $this->input->post('IdLocalizacao'),
			'IdInforOcorrencia' => $this->input->post('IdInforOcorrencia'),
			'IdUsuario' => $this->input->post('IdUsuario'),
			'IdAgente' => $this->input->post('IdAgente'),
			'Descricao' => $this->input->post('Descricao'),
			'Nivel' => $this->input->post('Nivel'),
			'Situacao' => $this->input->post('Situacao'),
			'DataInclusao' => date('Y-m-d H:i:s', time())
		);

		$this->db->insert('ocorrencia', $data);

	}
}

?>