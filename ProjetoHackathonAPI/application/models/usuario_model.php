<?php

class usuario_model extends CI_Model{

    public function __construct(){
		$this->load->database();
	}

	public function Get($id = false){
		if($id != true){
			$query = $this->db->get('usuario'); 
			return $query->result_array();
		}else{
			$query = $this->db->get_where("usuario", array('IdUsuario'=>$id));
			return $query->row_array();
		}
	}

	public function Set(){
		$data = array(
			'Nome' => $this->input->post('Nome'),
			'Email' => $this->input->post('Email'),
			'Senha' => $this->input->post('Senha'),
			'CPF' => $this->input->post('CPF'),
			'Sexo' => $this->input->post('Sexo'),
			'Idade' => $this->input->post('Idade'),
			'Telefone' => $this->input->post('Telefone')
		);

		$this->db->insert('usuario', $data);

	}
}

?>